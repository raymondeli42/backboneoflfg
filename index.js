var express = require("express")
var app = express()
var mess = []
app.get("/",function(request,response){
    response.send("Welcome to the back bone of the LFG APP.")
})
app.get("/n/:message?",function(request, response) {
    var message = request.params.message
    mess.push(message)
    response.send(["ok"])
})
app.get("/messages",function(request, response) {
    response.send(mess)
})
app.listen(process.env.PORT)